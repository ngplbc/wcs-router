import React from "react";
import { Router, Link, NavLink } from "react-router-dom";

import "./Navbar.css";


const Navbar = () => {
  return (
    <nav>
      <h1>Logo</h1>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><NavLink to="/our-history" activeStyle={{ color: "red" }}>History</NavLink></li>
      </ul>
    </nav>
  )
};
 
export default Navbar;
